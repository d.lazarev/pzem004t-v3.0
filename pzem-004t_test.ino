#include <HardwareSerial.h>
#include <PZEM004Tv30.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <NTPClient.h>
#include <WiFiUDP.h>
#include <ESPmDNS.h>
#include <ArduinoJson.h>
#include <esp_task_wdt.h>

#define LED_BUILTIN 2
#define WDT_TIMEOUT 10

typedef struct {
  float voltage;
  float current;
  float power;
  float energy;
  float frequency;
  float power_factor;  
} pzem004_data;

const char* ssid = "BOSON2.4";
const char* passwd = "kartoshka";

pzem004_data pzd;
void checkWebClient(void);
bool getPZEM004data(PZEM004Tv30*, pzem004_data*);
String& constructHTMLString(String&, pzem004_data*);
void sendHelloPage(WiFiClient*);
void sendInfoPage(WiFiClient*);
void sendErrorPage(WiFiClient*);
void sendJsonData(WiFiClient);
void resetEnergy(void);

WiFiServer server(80);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.com", 3600 * 3);

PZEM004Tv30 pzem0(&Serial), pzem1(&Serial1), pzem2(&Serial2);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, 0);
   
  // Connecting to WiFi
  WiFi.begin(ssid, passwd);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, 1);
    delay(500);
    digitalWrite(LED_BUILTIN, 0);
    delay(500);
    digitalWrite(LED_BUILTIN, 1);
    delay(500);
    digitalWrite(LED_BUILTIN, 0);
    delay(1500);
  }

  // Starting mDNS responder
  while(!MDNS.begin("esp32")) {
    digitalWrite(LED_BUILTIN, 1);
    delay(1000);
    digitalWrite(LED_BUILTIN, 0);      
    delay(1000);
  }

 // Starting HTTP server
 	server.begin();
 	MDNS.addService("http", "tcp", 80); 

 
	// Synchronize time
	timeClient.begin();
//	while(!timeClient.update()) {
//    digitalWrite(LED_BUILTIN, 1);
//    timeClient.forceUpdate();
//		delay(500);
//    digitalWrite(LED_BUILTIN, 0);
//		delay(500);
//  }

	esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
  esp_task_wdt_add(NULL); //add current thread to WDT watch
	
	digitalWrite(LED_BUILTIN, 1);
}

void loop() {
    checkWebClient();
		esp_task_wdt_reset();
    delay(300);
}

void checkWebClient() {
    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client) {
        return;
    }

    digitalWrite(LED_BUILTIN, 0); // Turn off led on client processing

		// Wait for data from client to become available
    while(client.connected() && !client.available()){
        delay(1);
    }

    // Read the first line of HTTP request
    String req = client.readStringUntil('\r');

    // First line of HTTP request looks like "GET /path HTTP/1.1"
    // Retrieve the "/path" part by finding the spaces
    int addr_start = req.indexOf(' ');
    int addr_end = req.indexOf(' ', addr_start + 1);
    if (addr_start == -1 || addr_end == -1) {
        return;
    }
    req = req.substring(addr_start + 1, addr_end);

    if (req == "/") 
    	sendHelloPage(&client);

    else if (req == "/pzem004") 
      sendInfoPage(&client);
		
		else if (req == "/pzem004-json")
			sendJsonData(client);

    else if (req == "/pzem004-resetEnergy") {
			resetEnergy();
			sendInfoPage(&client);
		}
		else 
        sendErrorPage(&client);
    
		client.stop();
    
		digitalWrite(LED_BUILTIN, 1); // Turn on led on client waiting
}

// +++++++++++++++++++++++++++++++++

/*
	Function send to the client JSON object:
	{
		"phase1": 
		{ 
			"voltage": 220.0,
			"current": 12.000,
			"power":	120.0,
			"energy":	2200.000,
			"frequency": 50.0,
			"power factor": 0.23
		},

		"phase2": 
		{ 
			"voltage": 220.0,
			"current": 12.000,
			"power":	120.0,
			"energy":	2200.000,
			"frequency": 50.0,
			"power factor": 0.23
		},

		"phase3": 
		{ 
			"voltage": 220.0,
			"current": 12.000,
			"power":	120.0,
			"energy":	2200.000,
			"frequency": 50.0,
			"power factor": 0.23
		}
	}
*/
void sendJsonData(WiFiClient client) {
	const size_t capacity = 4096;
	
	DynamicJsonDocument doc(capacity);
	JsonObject root = doc.to<JsonObject>();
	
	JsonObject phase1 = root.createNestedObject("phase1");
	JsonObject phase2 = root.createNestedObject("phase2");
	JsonObject phase3 = root.createNestedObject("phase3");
  
	getPZEM004data(&pzem0, &pzd);
	
	phase1["voltage"] = pzd.voltage;
	phase1["current"] = pzd.current;
	phase1["power"] = pzd.power;
	phase1["energy"] = pzd.energy;
	phase1["frequency"] = pzd.frequency;
	phase1["power factor"] = pzd.power_factor;

  getPZEM004data(&pzem1, &pzd);
	phase2["voltage"] = pzd.voltage;
	phase2["current"] = pzd.current;
	phase2["power"] = pzd.power;
	phase2["energy"] = pzd.energy;
	phase2["frequency"] = pzd.frequency;
	phase2["power factor"] = pzd.power_factor;
	
  getPZEM004data(&pzem2, &pzd);
	phase3["voltage"] = pzd.voltage;
	phase3["current"] = pzd.current;
	phase3["power"] = pzd.power;
	phase3["energy"] = pzd.energy;
	phase3["frequency"] = pzd.frequency;
	phase3["power factor"] = pzd.power_factor;
	
	root["ESP_getFreeHeap"] = ESP.getFreeHeap();
	root["Date"] = timeClient.getFormattedTime();

	String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n";
	client.print(s);
	serializeJsonPretty(root, client);	
}

void sendErrorPage(WiFiClient *client) {
	String s = "HTTP/1.1 404 Not Found\r\n\r\n";
	client->print(s);
}

void sendInfoPage(WiFiClient *client) {
	String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html><body>\r\n<h1>Latest data from pzem004</h1>\r\n";

	s += "<h2>Phase 1</h2>";
  getPZEM004data(&pzem0, &pzd);
  s = constructHTMLString(s, &pzd);
  s += "<h2>Phase 2</h2>";
  getPZEM004data(&pzem1, &pzd);
  s = constructHTMLString(s, &pzd);
  s += "<h2>Phase 3</h2>";
  getPZEM004data(&pzem2, &pzd);
  s = constructHTMLString(s, &pzd);

  s += "</body></html>";
  client->print(s);   

}

void sendHelloPage(WiFiClient *client) {
	IPAddress ip = WiFi.localIP();
	String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
	String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>Hello from ESP32 at ";
	s += ipStr;
  s += "</html>\r\n\r\n";
	client->print(s);
}

String& constructHTMLString(String &s, pzem004_data *pzd) {
  s += "<ul>";
  s += "<li>Voltage, V = " + String(pzd->voltage, 1) + "</li>";
  s += "<li>Current, A = " + String(pzd->current, 3) + "</li>";
  s += "<li>Power, W = " + String(pzd->power, 1) + "</li>";
  s += "<li>Energy, Wh = " + String(pzd->energy, 3) + "</li>";
  s += "<li>Frequency, Hz = " + String(pzd->frequency, 1) + "</li>";
  s += "<li>Power factor = " + String(pzd->power_factor, 2) + "</li>";
  s += "</ul>\r\n";
  return s;    
}

bool getPZEM004data(PZEM004Tv30 *pzem, pzem004_data *pz) {
	pz->voltage = pzem->voltage();
	pz->current = pzem->current();
  pz->power = pzem->power();
  pz->energy = pzem->energy();
  pz->frequency = pzem->frequency();
  pz->power_factor = pzem->pf();
  return true;
}

void resetEnergy() {
	pzem0.resetEnergy();
	pzem1.resetEnergy();
	pzem2.resetEnergy();
}
