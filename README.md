# pzem004t-v3.0

3 Phases current logger on pzem004t-v3.0 and esp32

To make Serial1 work on esp32, 
I edited the HardwareSerial.cpp file in the arduino built-in library.
I changed the define RX,TX for Serial1 from 10, 11 to 4,2 respectively.
I was guided by this page, thanks to the guys: https://rntlab.com/question/esp32-uart1/
By the way, UART on pzem works from 3.3v from esp32 wonderful. Without any alterations.
