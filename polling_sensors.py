#!/usr/bin/env python3
import requests
from influxdb import InfluxDBClient
import json
import time
from sendMail import sendMail


class Pzem004(object):
    in_proccess = False

    def poll(self):
        if self.in_proccess:
            return

        self.in_procccess = True

        try:
            response = requests.get('http://192.168.153.10/pzem004-json')
        except requests.exceptions.RequestException as err:
            sendMail("rasp4@perets.su", "dmitry@perets.su", "polling pzem004 sensor", str(err))

        e = response.json()

        client = InfluxDBClient(host='ldv80.perets.su', port=8086, timeout=30)
        client.switch_database('electrical')
#        client = DataFrameClient(host='perets.su', port=8089, use_udp=True)

        j = '['

        for ph in e:
            if ph[:5] == 'phase':
                j += '{"measurement": "' + ph + '", '
                j += '"fields": {'
                for field in e[ph]:
                    j += '"' + field + '":' + str(float(e[ph][field])) + ', '
                j = j[:-2]
                j += '},'
                j = j[:-1]
                j += '},'
        j = j[:-1]
        j += ']'

        j_insert = json.loads(j)
        try:
            client.write_points(j_insert)
        except Exception as e:
            print("write_point() exception: " + str(e))
#        client.send_packet(j_insert)

        self.in_proccess = False


if __name__ == "__main__":
    pzem = Pzem004()
    while True:
        pzem.poll()
        time.sleep(33)
