import os


def sendMail(frm, to, subj, message):
    sendmail_location = "/usr/sbin/sendmail" # sendmail location
    p = os.popen("%s -t" % sendmail_location, "w")
    p.write("From: %s\n" % frm)
    p.write("To: %s\n" % to)
    p.write("Subject: %s\n" % subj)
    p.write("\n") # blank line separating headers from body
    p.write(message)
    status = p.close()
    if status != 0:
           print("Sendmail exit status", status)
