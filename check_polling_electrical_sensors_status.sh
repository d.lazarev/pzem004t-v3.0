#!/bin/bash

SERVICE="polling_electrical_sensors"
STATUS="$(/usr/bin/systemctl is-active $SERVICE)"

if [ "$STATUS" != "active" ]; then
	echo "Service $SERVICE is not running"
	sudo /usr/bin/systemctl restart $SERVICE
	echo "Service $SERVICE has been restarted"
fi

